#! /usr/bin/env ruby

#   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU Library General Public License as
#   published by the Free Software Foundation; either version 3, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details
#
#   You should have received a copy of the GNU Library General Public
#   License along with this program; if not, write to the
#   Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

PRESENCE_FIELD_STRING = 'Valid'

class CodeGenerator
    class AttributeDeclaration
        attr_accessor :accessorName
        attr_accessor :modifierName
        attr_accessor :xmlName
        attr_accessor :variableName
        attr_accessor :typeName
        attr_accessor :documentation
        attr_accessor :type
        attr_accessor :values
    end

    class ModelDeclaration
        attr_accessor :className
        attr_accessor :classDocumentation
        attr_accessor :xmlClassName

        def initialize
            @attributes = []
        end

        def getEnumDeclarations()
            enumDeclarations = []
            
            @attributes.each do |oneAttribute|
                if oneAttribute.type == :enum
                    enumDeclarations << oneAttribute
                end
            end
            
            return enumDeclarations
        end

        def getAttributeTypes()
            attributeTypes = []
            
            @attributes.each do |oneAttribute|
                attributeTypes << oneAttribute.typeName
            end
            
            return attributeTypes
        end

        def getAttributes()
            @attributes
        end
    end

    class ChildListContainerModelDeclaration < ModelDeclaration    
        def set_child_list(variableName, typeName, accessorName, modifierName, documentation)
            @attributes << AttributeDeclaration.new
            @attributes.last.accessorName = accessorName
            if modifierName
                @attributes.last.modifierName = modifierName
            else
                @attributes.last.modifierName = "set#{accessorName.capitalize}"
            end
            @attributes.last.variableName = variableName
            @attributes.last.typeName = typeName
            @attributes.last.documentation = documentation
            @attributes.last.type = :childList
        end
    end

    class SimpleModelDeclaration < ModelDeclaration
        def add_scalar(variableName, typeName, accessorName, modifierName, xmlName, documentation)
            @attributes << AttributeDeclaration.new
            @attributes.last.accessorName = accessorName
            if modifierName
                @attributes.last.modifierName = modifierName
            else
                @attributes.last.modifierName = "set#{accessorName.capitalize}"
            end
            if xmlName
                @attributes.last.xmlName = xmlName
            else
                @attributes.last.xmlName = accessorName
            end
            @attributes.last.variableName = variableName
            @attributes.last.typeName = typeName
            @attributes.last.documentation = documentation
            @attributes.last.type = :scalar
        end

        def add_enum(variableName, typeName, accessorName, modifierName, values, xmlName, documentation)
            @attributes << AttributeDeclaration.new
            @attributes.last.accessorName = accessorName
            if modifierName
                @attributes.last.modifierName = modifierName
            else
                @attributes.last.modifierName = "set#{accessorName.capitalize}"
            end
            if xmlName
                @attributes.last.xmlName = xmlName
            else
                @attributes.last.xmlName = accessorName
            end
            @attributes.last.variableName = variableName
            @attributes.last.typeName = typeName
            @attributes.last.documentation = documentation
            @attributes.last.values = values
            @attributes.last.type = :enum
        end
    end

    class MixedModelDeclaration < SimpleModelDeclaration
        def add_child(variableName, typeName, xmlName, accessorName, modifierName, documentation)
            @attributes << AttributeDeclaration.new
            @attributes.last.accessorName = accessorName
            @attributes.last.xmlName = xmlName
            if modifierName
                @attributes.last.modifierName = modifierName
            else
                @attributes.last.modifierName = "set#{accessorName.capitalize}"
            end
            @attributes.last.variableName = variableName
            @attributes.last.typeName = typeName
            @attributes.last.documentation = documentation
            @attributes.last.type = :child
        end
    end

    def CodeGenerator.generateClass
        modelDefinition = SimpleModelDeclaration.new
        yield(modelDefinition)
        generateHeaderFile(modelDefinition)
        generateSourceFile(modelDefinition)
    end

    def CodeGenerator.generateChildListContainerClass
        modelDefinition = ChildListContainerModelDeclaration.new
        yield(modelDefinition)
        generateHeaderFile(modelDefinition)
        generateSourceFile(modelDefinition)
    end

    def CodeGenerator.generateMixedModelDeclarationClass
        modelDefinition = MixedModelDeclaration.new
        yield(modelDefinition)
        generateHeaderFile(modelDefinition)
        generateSourceFile(modelDefinition)
    end

    def CodeGenerator.generateHeaderFile(modelDefinition)
        fileName = "#{modelDefinition.className.downcase}.h"
        File.open(fileName, "w") do |theFile|
            generateHeaderHeader theFile, modelDefinition
            generateEnumDeclarations theFile, modelDefinition
            generateMethodDeclarations theFile, modelDefinition
            generateAttributeDeclarations theFile, modelDefinition
            generateHeaderFooter theFile, modelDefinition
        end
    end

    def CodeGenerator.isChildListContainerClass?(modelDefinition)
        modelDefinition.getAttributes().each do |oneAttribute|
            if oneAttribute.type == :childList
                return true
            else
                return false
            end
        end

        false
    end

    def CodeGenerator.generateChildInclude(modelDefinition)
        result = []
        modelDefinition.getAttributes().each do |oneAttribute|
            if oneAttribute.type == :childList || oneAttribute.type == :child
                result << "#include <#{oneAttribute.typeName.downcase}.h>"
            end
        end

        if result.empty?
            return ""
        else
            return "#{result.join("\n")}\n\n"
        end
    end

    def CodeGenerator.generateHeaderHeader(fileHandle, modelDefinition)
        textData = <<CODE_DATA
/**
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and\/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *\/

#if !defined #{modelDefinition.className.upcase}_H
#define #{modelDefinition.className.upcase}_H

#include "qtrocrail_export.h"

#{generateChildInclude(modelDefinition)}#{"#include <QList>\n" if isChildListContainerClass?(modelDefinition)}#include <QtGlobal>
#include <QString>
#include <QMetaType>

class QDomDocument;
class QDomNode;

namespace QtRocrail
{

#{
    if modelDefinition.classDocumentation && !modelDefinition.classDocumentation.empty?
        "/**\n * #{modelDefinition.classDocumentation.join("\n * ")}\n */"
    else
        ""
    end
}
class QTROCRAIL_EXPORT #{modelDefinition.className}
{
public:
CODE_DATA
        fileHandle.puts textData
    end

    def CodeGenerator.isPlainOldDataType?(dataType)
        case dataType.typeName
        when "bool"
            true
        when "int"
            true
        when "qlonglong"
            true
        else
            if dataType.type == :enum
                true
            else
                false
            end
        end
    end

    def CodeGenerator.generateEnumDeclarations(fileHandle, modelDefinition)
        modelDefinition.getEnumDeclarations.each do |oneEnum|
            fileHandle.puts "    enum #{oneEnum.typeName}\n    {\n        #{oneEnum.values.join(", ")}\n    };\n\n"
        end
    end

    def CodeGenerator.generateMethodDeclarations(fileHandle, modelDefinition)
        constructorParameters = []
        modelDefinition.getAttributes().each do |oneAttribute|
            if isPlainOldDataType? oneAttribute
                constructorParameters << oneAttribute.typeName
            elsif oneAttribute.type == :childList
                constructorParameters << "const QList<#{oneAttribute.typeName}>&"
            else
                constructorParameters << "const #{oneAttribute.typeName}&"
            end
        end
        fileHandle.puts "    explicit #{modelDefinition.className}(#{constructorParameters.join(", ")});\n\n"
        fileHandle.puts "    explicit #{modelDefinition.className}(const QDomNode&);\n\n"
        fileHandle.puts "    #{modelDefinition.className}();\n\n"
        fileHandle.puts "    void toXML(QDomDocument&, QDomNode&) const;\n\n"
        fileHandle.puts "    void fuse(const #{modelDefinition.className}&);\n\n"
        modelDefinition.getAttributes.each do |oneAttribute|
            if isPlainOldDataType? oneAttribute
                fileHandle.puts "    #{oneAttribute.typeName} #{oneAttribute.accessorName}() const\n    {\n        return #{oneAttribute.variableName};\n    }\n\n"
                fileHandle.puts "    void #{oneAttribute.modifierName}(#{oneAttribute.typeName} p#{oneAttribute.variableName})\n"
                fileHandle.puts "    {\n        #{oneAttribute.variableName} = p#{oneAttribute.variableName};\n        #{oneAttribute.variableName}#{PRESENCE_FIELD_STRING} = 1;\n    }\n\n"
            elsif oneAttribute.type == :childList
                fileHandle.puts "    const QList<#{oneAttribute.typeName}>& #{oneAttribute.accessorName}() const\n    {\n        return #{oneAttribute.variableName};\n    }\n\n"
                fileHandle.puts "    void #{oneAttribute.modifierName}(const QList<#{oneAttribute.typeName}> &p#{oneAttribute.variableName})\n"
                fileHandle.puts "    {\n        #{oneAttribute.variableName} = p#{oneAttribute.variableName};\n    }\n\n"
            elsif oneAttribute.type == :child
                fileHandle.puts "    bool #{oneAttribute.accessorName}Valid() const\n    {\n        return #{oneAttribute.variableName}#{PRESENCE_FIELD_STRING};\n    }\n\n"
                fileHandle.puts "    const #{oneAttribute.typeName}& #{oneAttribute.accessorName}() const\n    {\n        return #{oneAttribute.variableName};\n    }\n\n"
                fileHandle.puts "    void #{oneAttribute.modifierName}(const #{oneAttribute.typeName} &p#{oneAttribute.variableName})\n"
                fileHandle.puts "    {\n        #{oneAttribute.variableName} = p#{oneAttribute.variableName};\n    }\n\n"
            else
                fileHandle.puts "    const #{oneAttribute.typeName}& #{oneAttribute.accessorName}() const\n    {\n        return #{oneAttribute.variableName};\n    }\n\n"
                fileHandle.puts "    void #{oneAttribute.modifierName}(const #{oneAttribute.typeName} &p#{oneAttribute.variableName})\n"
                fileHandle.puts "    {\n        #{oneAttribute.variableName} = p#{oneAttribute.variableName};\n        #{oneAttribute.variableName}#{PRESENCE_FIELD_STRING} = 1;\n    }\n\n"
            end
        end
    end

    def CodeGenerator.generateAttributeDeclarations(fileHandle, modelDefinition)
        fileHandle.puts "private:\n\n"
        modelDefinition.getAttributes.each do |oneAttribute|
            if oneAttribute.documentation
                fileHandle.puts "    /**\n"
                fileHandle.puts "     * #{oneAttribute.documentation}\n"
                fileHandle.puts "     */\n"
            end
            if oneAttribute.type == :childList
                fileHandle.puts "    QList<#{oneAttribute.typeName}> #{oneAttribute.variableName};\n\n"
            else
                fileHandle.puts "    #{oneAttribute.typeName} #{oneAttribute.variableName};\n\n"
            end
        end
        modelDefinition.getAttributes.each do |oneAttribute|
            if oneAttribute.type != :childList
                fileHandle.puts "    /**\n"
                fileHandle.puts "     * Is the field #{oneAttribute.variableName} is valid\n"
                fileHandle.puts "     */\n"
                fileHandle.puts "    int #{oneAttribute.variableName}#{PRESENCE_FIELD_STRING}:1;\n\n"
            end
        end
    end

    def CodeGenerator.generateHeaderFooter(fileHandle, modelDefinition)
        fileHandle.puts "};\n\n}\n\n"
        fileHandle.puts "Q_DECLARE_METATYPE(QtRocrail::#{modelDefinition.className})\n\n"
        fileHandle.puts "#endif\n\n"
    end

    def CodeGenerator.generateSourceFile(modelDefinition)
        fileName = "#{modelDefinition.className.downcase}.cpp"
        File.open(fileName, "w") do |theFile|
            generateSourceHeader theFile, modelDefinition
            generateConversionMethods theFile, modelDefinition
            generateDefaultConstructor theFile, modelDefinition
            generateNormalConstructor theFile, modelDefinition
            generateXmlConstructor theFile, modelDefinition
            generateToXmlMethod theFile, modelDefinition
            generateFuseMethod theFile, modelDefinition
            generateSourceFooter theFile, modelDefinition
        end
    end

    def CodeGenerator.generateSourceHeader(fileHandle, modelDefinition)
        textData = <<CODE_DATA
/**
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and\/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *\/

#include "#{modelDefinition.className.downcase}.h"

#include <QDomDocument>
#include <QDomNode>
#include <QDebug>

namespace QtRocrail
{

CODE_DATA
        fileHandle.puts textData
    end

    def CodeGenerator.generateMethod(padding, name, className, returnValue, parameters, body, properties, initialisationValue)
        resultString = "#{padding}"

        resultString << "static " if properties.include? :static
        if returnValue
            resultString << "#{returnValue} "
        end
        if className
            resultString << "#{className}::"
        end
        resultString << "#{name}(#{parameters.join(", ")})"
        resultString << " const" if properties.include? :const
        resultString << "\n"
        if initialisationValue && !initialisationValue.empty?
            resultString << "#{padding}    : #{initialisationValue.join(", ")}\n"
        end
        resultString << "#{padding}{\n"
        resultString << "#{padding}    #{body.join("\n#{padding}    ")}\n" if !body.empty?
        resultString << "#{padding}}\n\n"
        return resultString
    end

    def CodeGenerator.generateDefaultConstructor(fileHandle, modelDefinition)
        initialisationValue = []
        modelDefinition.getAttributes.each do |oneAttribute|
            initialisationValue << "#{oneAttribute.variableName}()"
        end
        modelDefinition.getAttributes.each do |oneAttribute|
            initialisationValue << "#{oneAttribute.variableName}#{PRESENCE_FIELD_STRING}(0)" if oneAttribute.type != :childList
        end
        fileHandle.write generateMethod("", modelDefinition.className, modelDefinition.className, nil, [], [], [], initialisationValue)
    end

    def CodeGenerator.generateNormalConstructor(fileHandle, modelDefinition)
        parametersDeclaration = []
        initialisationValue = []
        modelDefinition.getAttributes.each do |oneAttribute|
            if isPlainOldDataType? oneAttribute
                parametersDeclaration << "#{oneAttribute.typeName} p#{oneAttribute.variableName}"
            elsif oneAttribute.type == :childList
                parametersDeclaration << "const QList<#{oneAttribute.typeName}> &p#{oneAttribute.variableName}"
            else
                parametersDeclaration << "const #{oneAttribute.typeName} &p#{oneAttribute.variableName}"
            end
            initialisationValue << "#{oneAttribute.variableName}(p#{oneAttribute.variableName})"
        end
        modelDefinition.getAttributes.each do |oneAttribute|
            initialisationValue << "#{oneAttribute.variableName}#{PRESENCE_FIELD_STRING}(1)" if oneAttribute.type != :childList
        end
        fileHandle.write generateMethod("", modelDefinition.className, modelDefinition.className, nil, parametersDeclaration, [], [], initialisationValue)
    end

    def CodeGenerator.generateXmlConstructor(fileHandle, modelDefinition)
        parametersDeclaration = ["const QDomNode &source"]
        initialisationValue = []
        modelDefinition.getAttributes.each do |oneAttribute|
            initialisationValue << "#{oneAttribute.variableName}()"
        end
        body = []
        body  << "if (source.nodeName() == QString::fromLatin1(\"#{modelDefinition.xmlClassName}\"))"
        body  << "{"
        modelDefinition.getAttributes.each do |oneAttribute|
            body << "    {"
            if oneAttribute.type == :childList
                body << "        const QDomNodeList &allChilds(source.childNodes());"
                body << "        const int childCount = allChilds.count();"
                body << "        for (int i = 0; i < childCount; ++i) {"
                body << "            #{oneAttribute.variableName}.push_back(#{oneAttribute.typeName}(allChilds.at(i)));"
                body << "        }"
            elsif oneAttribute.type == :child
                body << "        const QDomNodeList &allChilds(source.childNodes());"
                body << "        const int childCount = allChilds.count();"
                body << "        for (int i = 0; i < childCount; ++i) {"
                body << "            if (allChilds.at(i).nodeName() == \"#{oneAttribute.xmlName}\") {"
                body << "                #{oneAttribute.variableName} = #{oneAttribute.typeName}(allChilds.at(i));"
                body << "                break;"
                body << "            }"
                body << "        }"
            else
                body << "        const QString &attributeName(QString::fromLatin1(\"#{oneAttribute.xmlName}\"));"
                body << "        if (source.attributes().contains(attributeName))"
                body << "        {"
                body << "            #{oneAttribute.variableName} = #{generateFromStringToTypeConversion(oneAttribute)};"
                body << "            #{oneAttribute.variableName}#{PRESENCE_FIELD_STRING} = 1;"
                body << "        }"
            end
            body << "    }"
        end
        body  << "}"
        fileHandle.write generateMethod("", modelDefinition.className, modelDefinition.className, nil, parametersDeclaration, body, [], initialisationValue)
    end

    def CodeGenerator.generateToXmlMethod(fileHandle, modelDefinition)
        parametersDeclaration = ["QDomDocument &document", "QDomNode &rootNode"]
        body = []
        body  << "QDomElement modelXmlQuery(document.createElement(QString::fromLatin1(\"#{modelDefinition.xmlClassName}\")));"
        modelDefinition.getAttributes.each do |oneAttribute|
            if oneAttribute.type == :childList
                body << "for (QList<#{oneAttribute.typeName}>::const_iterator itElt = #{oneAttribute.variableName}.begin(); itElt != #{oneAttribute.variableName}.end(); ++itElt) {"
                body << "    itElt->toXML(document, modelXmlQuery);"
                body << "}"
            elsif oneAttribute.type == :child
                body << "#{oneAttribute.variableName}.toXML(document, modelXmlQuery);"
            else
                body << "if (#{oneAttribute.variableName}#{PRESENCE_FIELD_STRING}) {"
                body << "    modelXmlQuery.setAttribute(QString::fromLatin1(\"#{oneAttribute.xmlName}\"), #{generateToStringFromoTypeConversion(oneAttribute)});"
                body << "}"
            end
        end
        body  << "rootNode.appendChild(modelXmlQuery);"
        fileHandle.write generateMethod("", "toXML", modelDefinition.className, "void", parametersDeclaration, body, [:const], [])
    end

    def CodeGenerator.generateFuseMethod(fileHandle, modelDefinition)
        parametersDeclaration = ["const #{modelDefinition.className} &other"]
        body = []
        modelDefinition.getAttributes.each do |oneAttribute|
            if oneAttribute.type == :childList
#                body << "for (QList<#{oneAttribute.typeName}>::const_iterator itElt = #{oneAttribute.variableName}.begin(); itElt != #{oneAttribute.variableName}.end(); ++itElt) {"
#                body << "    for (QList<#{oneAttribute.typeName}>::const_iterator itOtherElt = other.#{oneAttribute.variableName}.begin(); itOtherElt != other.#{oneAttribute.variableName}.end(); ++itOtherElt) {"
#                body << "        if (itOtherElt->"
#                body << "    }"
#                body << "}"
            else
                body << "if (other.#{oneAttribute.variableName}#{PRESENCE_FIELD_STRING}) {"
                body << "    #{oneAttribute.modifierName}(other.#{oneAttribute.accessorName}());"
                body << "}"
            end
        end
       fileHandle.write generateMethod("", "fuse", modelDefinition.className, "void", parametersDeclaration, body, [], [])
    end

    def CodeGenerator.generateConversionMethods(fileHandle, modelDefinition)
        modelDefinition.getAttributes.each do |oneAttribute|
            if oneAttribute.type == :enum
                parametersDeclaration = ["#{modelDefinition.className}::#{oneAttribute.typeName} value"]
                body = []
                body << "switch(value)"
                body << "{"
                oneAttribute.values.each do |enumValue|
                    body << "case #{modelDefinition.className}::#{enumValue}:"
                    body << "    return QLatin1String(\"#{enumValue}\");"
                    body << "    break;"
                end
                body  << "}\n"
                body  << "return QLatin1String(\"\");"
                fileHandle.write generateMethod("", "toString", nil, "QLatin1String", parametersDeclaration, body, [:static], [])

                parametersDeclaration = ["const QString &value"]
                body = []
                oneAttribute.values.each do |enumValue|
                    if enumValue != "invalid"
                        body << "if (value == QLatin1String(\"#{enumValue}\")) {"
                        body << "    return #{modelDefinition.className}::#{enumValue};"
                        body << "}"
                    end
                end
                body << "return #{modelDefinition.className}::invalid;"

                fileHandle.write generateMethod("", "fromString", nil, "#{modelDefinition.className}::#{oneAttribute.typeName}", parametersDeclaration, body, [:static], [])
            end
        end
    end

    def CodeGenerator.generateFromStringToTypeConversion(attribute)
        valueAccess = "source.attributes().namedItem(attributeName).nodeValue()"
        case attribute.typeName
        when "bool"
            "#{valueAccess} == \"true\" ? true : false"
        when "int"
            "#{valueAccess}.toInt()"
        when "qlonglong"
            "#{valueAccess}.toLongLong()"
        when "QString"
            "#{valueAccess}"
        else
            if attribute.type == :enum
                "fromString(#{valueAccess})"
            elsif attribute.type == :childList
                "#{attribute.typeName}(#{valueAccess})"
            elsif attribute.type == :child
                "#{attribute.typeName}(#{valueAccess})"
            else
                puts "unknown type conversion #{attribute.typeName}"
            end
        end
    end

    def CodeGenerator.generateToStringFromoTypeConversion(attribute)
        case attribute.typeName
        when "bool"
            "#{attribute.accessorName}() ? QLatin1String(\"true\") : QLatin1String(\"false\")"
        when "int"
            "#{attribute.accessorName}()"
        when "qlonglong"
            "#{attribute.accessorName}()"
        when "QString"
            "#{attribute.accessorName}()"
        else
            if attribute.type == :enum
                "toString(#{attribute.accessorName}())"
            elsif attribute.type == :childList
                "#{attribute.typeName}(#{attribute.accessorName})"
            elsif attribute.type == :child
                "#{attribute.typeName}(#{attribute.accessorName})"
            else
                puts "unknown type conversion #{attribute.typeName}"
            end
        end
    end
    
    def CodeGenerator.generateSourceFooter(fileHandle, modelDefinition)
        fileHandle.puts "}\n\n"
    end
end
