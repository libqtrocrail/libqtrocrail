/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "shellpluginloader.h"

#include <KDebug>

TrainCommandPluginLoader::TrainCommandPluginLoader() : Plasma::PluginLoader()
{
}

KPluginInfo::List TrainCommandPluginLoader::internalAppletInfo(const QString &category) const
{
    kDebug() << category;
    return Plasma::PluginLoader::internalAppletInfo(category);
}

KPluginInfo::List TrainCommandPluginLoader::internalDataEngineInfo() const
{
    kDebug() << "";
    return Plasma::PluginLoader::internalDataEngineInfo();
}

Plasma::Applet* TrainCommandPluginLoader::internalLoadApplet(const QString &name, uint appletId, const QVariantList &args)
{
    kDebug() << name << appletId << args;
    return Plasma::PluginLoader::internalLoadApplet(name, appletId, args);
}

Plasma::DataEngine* TrainCommandPluginLoader::internalLoadDataEngine(const QString &name)
{
    kDebug() << name;
    return Plasma::PluginLoader::internalLoadDataEngine(name);
}

Plasma::AbstractRunner* TrainCommandPluginLoader::internalLoadRunner(const QString &name)
{
    kDebug() << name;
    return Plasma::PluginLoader::internalLoadRunner(name);
}

Plasma::Service* TrainCommandPluginLoader::internalLoadService(const QString &name, const QVariantList &args, QObject *parent)
{
    kDebug() << name << args;
    return Plasma::PluginLoader::internalLoadService(name, args, parent);
}

KPluginInfo::List TrainCommandPluginLoader::internalRunnerInfo() const
{
    kDebug() << "";
    return Plasma::PluginLoader::internalRunnerInfo();
}

KPluginInfo::List TrainCommandPluginLoader::internalServiceInfo() const
{
    kDebug() << "";
    return Plasma::PluginLoader::internalServiceInfo();
}
