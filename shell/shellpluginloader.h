/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#if !defined SHELLPLUGINLOADER_H
#define SHELLPLUGINLOADER_H

#include <KDE/Plasma/PluginLoader>

class TrainCommandPluginLoader : public Plasma::PluginLoader
{
public:

    TrainCommandPluginLoader();

protected:

    KPluginInfo::List internalAppletInfo (const QString &category) const;

    KPluginInfo::List internalDataEngineInfo () const;

    Plasma::Applet *internalLoadApplet (const QString &name, uint appletId=0, const QVariantList &args=QVariantList());

    Plasma::DataEngine *internalLoadDataEngine (const QString &name);

    Plasma::AbstractRunner *internalLoadRunner (const QString &name);

    Plasma::Service * internalLoadService (const QString &name, const QVariantList &args, QObject *parent=0);

    KPluginInfo::List internalRunnerInfo () const;

    KPluginInfo::List internalServiceInfo () const;

};

#endif // SHELLPLUGINLOADER_H
