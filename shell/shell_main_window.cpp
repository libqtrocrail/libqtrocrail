/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "shell_main_window.h"

#include "shellpluginloader.h"

#include <KDE/KService>
#include <KDE/KMessageBox>
#include <KDE/KApplication>
#include <KDE/KStandardAction>
#include <KDE/KActionCollection>
#include <KDE/KAction>
#include <KDE/Plasma/PluginLoader>

#include <KDE/KDebug>

#include <QCoreApplication>

TrainCommandMainWindow::TrainCommandMainWindow(QWidget *parent, Qt::WindowFlags f)
    : KParts::MainWindow(parent, f)
{
    setStandardToolBarMenuEnabled(true);
    createStandardStatusBarAction();
    setXMLFile("TrainCommandui.rc");

    KAction* action = KStandardAction::quit(qApp, SLOT(quit()), actionCollection());

    action = new KAction("&Configure", actionCollection());
    connect(action, SIGNAL(triggered()), this, SLOT(optionsPreferences()));
    actionCollection()->addAction(QLatin1String("options_configure"), action);

    action = new KAction("&Add Clock Applet", actionCollection());
    connect(action, SIGNAL(triggered()), this, SLOT(addClockApplet()));
    actionCollection()->addAction(QLatin1String("options_configure"), action);

    TrainCommandPluginLoader *pluginLoader = new TrainCommandPluginLoader;
    Plasma::PluginLoader::setPluginLoader(pluginLoader);
    QVariantList args;
    args << qVariantFromValue(static_cast<Plasma::PluginLoader*>(pluginLoader)) << "Online Services";
    loadKPart(args);

    connect(this, SIGNAL(addNewApplet(QString)), m_part, SLOT(addApplet(QString)));

    setAutoSaveSettings();
}

TrainCommandMainWindow::~TrainCommandMainWindow()
{
}

void TrainCommandMainWindow::loadKPart(const QVariantList &args)
{
    // this routine will find and load our Part.  it finds the Part by
    // name which is a bad idea usually.. but it's alright in this
    // case since our Part is made for this Shell
    KService::Ptr service = KService::serviceByDesktopPath( "plasma-kpart.desktop" );

    if (service) {
        // now that the Part is loaded, we cast it to a Part to get
        // our hands on it
        m_part = service->createInstance<KParts::ReadOnlyPart>(0, args);
        if (m_part) {
            // tell the KParts::MainWindow that this is indeed the main widget
            // If you have something better to do with the widget, this is where
            // you would do it.
            setCentralWidget(m_part->widget());

            // and integrate the part's GUI with the shell's
            createGUI(m_part);

        } else {
            // For whatever reason the part didn't load
            KMessageBox::error(this, i18n("Could not instantiate our Part!"));
            qApp->quit();
        }
    } else {
        // if we couldn't find our Part, we exit since the Shell by
        // itself can't do anything useful
        KMessageBox::error(this, i18n("Could not find our Part!"));
        qApp->quit();
        // we return here, cause qApp->quit() only means "exit the
        // next time we enter the event loop...
        return;
    }
}

void TrainCommandMainWindow::optionsPreferences()
{
    kDebug() << "";
}

void TrainCommandMainWindow::addClockApplet()
{
    emit(addNewApplet("Clock"));
}

