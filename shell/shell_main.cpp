/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "shell_main_window.h"

#include <KDE/KApplication>
#include <KDE/KCmdLineArgs>
#include <KDE/KAboutData>

int main(int argc, char *argv[])
{
    KAboutData myAbout("TrainCommand",
                       "train_command",
                       ki18nc("Application name", "Train Command"),
                       "0.1",
                       ki18nc("short description of the application", "Train Command can control train model using a server connected to specific hardware"),
                       KAboutData::License_GPL_V3,
                       ki18nc("copyright", "Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>"));

    myAbout.addAuthor(ki18nc("author name", "Matthieu Gallien"), ki18nc("task in project", "Creator"), "matthieu_gallien@yahoo.fr");

    KCmdLineArgs::init(argc, argv, &myAbout);

    KApplication theApp(true);

    // see if we are starting with session management
    if (theApp.isSessionRestored()) {
        RESTORE(TrainCommandMainWindow)
    } else {
        TrainCommandMainWindow *mainWindow = new TrainCommandMainWindow;
        mainWindow->show();
    }

    return theApp.exec();
}
