/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "locomotivestate.h"
#include "serverstate.h"
#include "locomotiveservice.h"
#include "locomotivejob.h"

LocomotiveService::LocomotiveService(QSharedPointer<QtRocrail::LocomotiveState> locomotiveState, QObject *parent) :
    Plasma::Service(parent), mLocomotiveState(locomotiveState)
{
    setName("trainmodel.locomotive");
}

Plasma::ServiceJob* LocomotiveService::createJob(const QString &operation, QMap<QString, QVariant> &parameters)
{
    return new LocomotiveJob(mLocomotiveState, mLocomotiveState->locomotive().id(), operation, parameters, this);
}
