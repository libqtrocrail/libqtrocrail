/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "traindataengine.h"

#include "locomotivelistsource.h"
#include "locomotivesource.h"
#include "locomotiveservice.h"

#include "locomotiveliststate.h"
#include "serverstate.h"

#include <QRegExp>
#include <KDebug>

TrainDataEngine::TrainDataEngine(QObject *parent, const QList<QVariant> &parameters) :
    Plasma::DataEngine(parent)
{
    setObjectName(QLatin1String("trainmodel"));
    Q_UNUSED(parameters);
}

Plasma::Service* TrainDataEngine::serviceForSource(const QString &source)
{
    QRegExp parseServerLocomotiveListSource("locomotiveList\\:address\\:(.*)");
    QRegExp parseServerLocomotiveSource("locomotive\\:(.*)\\:(.*)");

    kDebug() << "sourceRequestEvent(const QString &source)" << source;

    if (parseServerLocomotiveListSource.indexIn(source) == 0) {
    }

    if (parseServerLocomotiveSource.indexIn(source) == 0) {
        Plasma::DataContainer *container = containerForSource(source);
        LocomotiveSource *sourcePointer = qobject_cast<LocomotiveSource*>(container);

        return new LocomotiveService(sourcePointer->locomotiveState());
    }

    return Plasma::DataEngine::serviceForSource(source);
}

bool TrainDataEngine::sourceRequestEvent(const QString &source)
{
    QRegExp parseServerLocomotiveListSource("locomotiveList\\:address\\:(.*)");
    QRegExp parseServerLocomotiveSource("locomotive\\:(.*)\\:(.*)");

    kDebug() << "sourceRequestEvent(const QString &source)" << source;

    if (parseServerLocomotiveListSource.indexIn(source) == 0) {
        const QStringList &matches = parseServerLocomotiveListSource.capturedTexts();
        const QString &serverAddress(matches.at(1));
        kDebug() << "found locomotive source" << matches.at(1);

        QSharedPointer<QtRocrail::ServerState> serverPointer(mServers.getComponent(serverAddress, serverAddress));
        QSharedPointer<QtRocrail::LocomotiveListState> sourceData(new QtRocrail::LocomotiveListState(serverPointer));
        if (mLocomotiveLists.contains(serverAddress)) {
            if (mLocomotiveLists[serverAddress].isNull()) {
                mLocomotiveLists[serverAddress] = sourceData;
            }
        } else {
            mLocomotiveLists[serverAddress] = sourceData;
        }
        Q_ASSERT(!serverPointer.isNull());
        addSource(new LocomotiveListSource(serverAddress, sourceData, this));
        return true;
    }

    if (parseServerLocomotiveSource.indexIn(source) == 0) {
        const QStringList &matches = parseServerLocomotiveSource.capturedTexts();
        const QString &locomotiveName(matches.at(1));
        const QString &serverAddress(matches.at(2));
        kDebug() << "found locomotive source" << matches.at(1);

        QSharedPointer<QtRocrail::LocomotiveListState> locomotiveListPointer;
        if (mLocomotiveLists.contains(serverAddress)) {
            if (!mLocomotiveLists[serverAddress].isNull()) {
                locomotiveListPointer = mLocomotiveLists[serverAddress];
            }
        }

        QScopedPointer<QtRocrail::LocomotiveState> locomotiveStatePointer(new QtRocrail::LocomotiveState(mServers.getComponent(serverAddress, serverAddress),
                                                                                                         locomotiveListPointer,
                                                                                                         locomotiveName));
        addSource(new LocomotiveSource(serverAddress, locomotiveName, locomotiveStatePointer.take(), this));
        return true;
    }

    return false;
}

bool TrainDataEngine::updateSourceEvent(const QString& source)
{
    Q_UNUSED(source);
    return false;
}

K_EXPORT_PLASMA_DATAENGINE(train_model, TrainDataEngine)
