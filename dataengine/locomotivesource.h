/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef LOCOMOTIVESOURCE_H
#define LOCOMOTIVESOURCE_H

#include <locomotive.h>
#include <locomotivestate.h>

#include <Plasma/DataContainer>
#include <QSharedPointer>

namespace QtRocrail
{
class LocomotiveState;
}

class LocomotiveSource : public Plasma::DataContainer
{
    Q_OBJECT

public:
    explicit LocomotiveSource(const QString&, const QString&, QtRocrail::LocomotiveState*, QObject *parent = 0);

    virtual ~LocomotiveSource();

    QSharedPointer<QtRocrail::LocomotiveState> locomotiveState() const
    {
        return currentLocomotiveState;
    }

public slots:

    void locomotiveIsModified(const QtRocrail::Locomotive&);

signals:

    void sourceIsValid();

    void sourceIsInvalid();

private slots:

    void askLocomotiveInformation();

private:

    QSharedPointer<QtRocrail::ServerState> serverConnection;

    QSharedPointer<QtRocrail::LocomotiveState> currentLocomotiveState;

    QtRocrail::Locomotive currentLocomotive;

    QString uniqueName;
};

#endif // LocomotiveSource_H
