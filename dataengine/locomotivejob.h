/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef LOCOMOTIVEJOB_H
#define LOCOMOTIVEJOB_H

#include <Plasma/ServiceJob>
#include <QSharedPointer>

namespace QtRocrail
{
class LocomotiveState;
class ServerState;
};

class LocomotiveJob : public Plasma::ServiceJob
{
    Q_OBJECT

public:

    LocomotiveJob(QSharedPointer<QtRocrail::LocomotiveState> locomotiveState, const QString &destination,
                  const QString &operation, const QMap<QString, QVariant> &parameters, QObject *parent = 0);

    void start();

private:

    QSharedPointer<QtRocrail::LocomotiveState> mLocomotiveState;

    QSharedPointer<QtRocrail::ServerState> mServerConnection;

};

#endif // LOCOMOTIVEJOB_H
