import QtQuick 1.0
import org.kde.plasma.core 0.2 as PlasmaCore
 
Item {
    property string serverAddress: "127.0.0.1"

    PlasmaCore.Theme {
        id: theme
    }

    PlasmaCore.DataSource {
        id: locomotivesSource
        engine: "org.mgallien.trainmodel"
        interval: 0
        property string firstLocomotive

        Component.onCompleted: {
            connectedSources = "locomotiveList:address:" + serverAddress
        }
        onNewData: {
            print("here a data: " + data)
            for (var dataEntry in data)
            {
                locomotivesDetailSource.connectedSources += data[dataEntry]
            }
        }
    }

    PlasmaCore.DataSource {
        id: locomotivesDetailSource
        engine: "org.mgallien.trainmodel"
        interval: 0
        Component.onCompleted: {
            print("keys: " + locomotivesSource.firstLocomotive)
        }
    }
    
    ListView {
        anchors.fill: parent
        model: PlasmaCore.DataModel {
            dataSource: locomotivesDetailSource
        }

        delegate:
            Item {
                id: locomotiveDelegate
                width: parent.width
                height: 80

                PlasmaCore.FrameSvgItem {
                    id: delegateItem
                    imagePath: "widgets/frame"
                    prefix: "plain"
                    enabledBorders: PlasmaCore.FrameSvg.AllBorders
                    width: locomotiveDelegate.width
                    height: 80

                    Text {
                        id: locomotiveName
                        width: parent.width
                        color: theme.textColor
                        text: id + " at speed " + speed

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                var sourceName = "locomotive:" + id + ":" + serverAddress;
                                var operationDescription = locomotivesDetailSource.serviceForSource(sourceName).operationDescription("SetSpeed");
                                operationDescription.speed = 37;
                                locomotivesDetailSource.serviceForSource("locomotive:" + id + ":" + serverAddress).startOperationCall(operationDescription);
                            }
                        }
                    }
                    Rectangle {
                        id: forwardIndicator
                        width: parent.width
                        height: 20
                        anchors.top: locomotiveName.bottom
                        color: {
                            if (isForward) {
                                "green"
                            } else {
                                "red"
                            }
                        }
                    }
                    Rectangle {
                        width: parent.width
                        height: 20
                        anchors.top: forwardIndicator.bottom
                        color: {
                            if (lightOn) {
                                "green"
                            } else {
                                "red"
                            }
                        }
                    }
                }
            }
    }
}
