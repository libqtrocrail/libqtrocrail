/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "locomotivestate.h"
#include "serverstate.h"
#include "locomotiveliststate.h"
#include "remotequery.h"

#include <QStateMachine>
#include <QDebug>

namespace QtRocrail
{

LocomotiveState::LocomotiveState(QSharedPointer<QtRocrail::ServerState> serverObject,
                                 QWeakPointer<QtRocrail::LocomotiveListState> listPointer,
                                 const QString &locomotiveId, QObject *parent)
    : QObject(parent), serverPointer(serverObject), locomotiveListPointer(listPointer), id(locomotiveId),
      mLocomotive(), mIsValid(false), automaton(new QStateMachine(this))
{
    qDebug() << "LocomotiveState::LocomotiveState";
    mLocomotive.setId(id);

    if (locomotiveListPointer.data() != 0) {
        qDebug() << "LocomotiveState::LocomotiveState" << "locomotiveList pointer is valid";
        QSharedPointer<QtRocrail::LocomotiveListState> localLocomotiveListPointer(locomotiveListPointer);
        if (localLocomotiveListPointer->locomotiveExists(id)) {
            qDebug() << "LocomotiveState::LocomotiveState" << "locomotive exists:" << id;
            newLocomotiveMessage(localLocomotiveListPointer->locomotive(id));
        }
    }

    connect(serverPointer.data(), SIGNAL(newLocomotiveMessage(QtRocrail::Locomotive)), this, SLOT(newLocomotiveMessage(QtRocrail::Locomotive)));

    buildAutomaton();
}

LocomotiveState::~LocomotiveState()
{
}

void LocomotiveState::buildAutomaton()
{
    QState *locomotiveListIsNotValidAndLocomotiveIsNotValid = new QState;
    automaton->addState(locomotiveListIsNotValidAndLocomotiveIsNotValid);

    QState *locomotiveListIsNotValidAndLocomotiveIsValid = new QState;
    automaton->addState(locomotiveListIsNotValidAndLocomotiveIsValid);

    if (locomotiveListPointer.data() != 0) {
        QState *locomotiveListIsValid = new QState;
        automaton->addState(locomotiveListIsValid);
        automaton->setInitialState(locomotiveListIsValid);
        locomotiveListIsValid->addTransition(locomotiveListPointer.data(), SIGNAL(destroyed()), locomotiveListIsNotValidAndLocomotiveIsNotValid);
    } else {
        automaton->setInitialState(locomotiveListIsNotValidAndLocomotiveIsNotValid);
    }

    locomotiveListIsNotValidAndLocomotiveIsNotValid->addTransition(this, SIGNAL(listIsValid()), locomotiveListIsNotValidAndLocomotiveIsValid);
    locomotiveListIsNotValidAndLocomotiveIsValid->addTransition(this, SIGNAL(listIsValid()), locomotiveListIsNotValidAndLocomotiveIsNotValid);
}

bool LocomotiveState::isValid() const
{
    return mIsValid;
}

const Locomotive& LocomotiveState::locomotive() const
{
    return mLocomotive;
}

void LocomotiveState::newLocomotiveMessage(const QtRocrail::Locomotive &locomotive)
{
    qDebug() << "LocomotiveState::newLocomotiveMessage:" << locomotive.id();
    if (locomotive.id() == mLocomotive.id()) {
        qDebug() << "LocomotiveState::newLocomotiveMessage:" << "update the locomotive";
        mLocomotive = locomotive;
        mIsValid = true;
        emit validChanged();
        emit listIsValid();
        emit locomotiveModified(mLocomotive);
    }
}

void LocomotiveState::connectionReady()
{
    serverPointer->sendServerQuery(RemoteQuery::fromQuery(ModelQuery(ModelQuery::lcprops, Locomotive())));
}

void LocomotiveState::connectionLost()
{
    mIsValid = false;
    emit validChanged();
    emit listIsInvalid();
}

void LocomotiveState::stateChangeLocomotiveListInvalidLocomotiveInvalid()
{

}

void LocomotiveState::stateChangeLocomotiveListInvalidLocomotiveValid()
{

}

QWeakPointer<QtRocrail::ServerState> LocomotiveState::serverConnection() const
{
    return serverPointer;
}

}
