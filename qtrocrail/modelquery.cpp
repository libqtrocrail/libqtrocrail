/**
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "modelquery.h"

#include <QDomDocument>
#include <QDomNode>
#include <QDebug>

namespace QtRocrail
{

static QLatin1String toString(ModelQuery::Command value)
{
    switch(value)
    {
    case ModelQuery::add:
        return QLatin1String("add");
        break;
    case ModelQuery::addmodule:
        return QLatin1String("addmodule");
        break;
    case ModelQuery::bklist:
        return QLatin1String("bklist");
        break;
    case ModelQuery::fstat:
        return QLatin1String("fstat");
        break;
    case ModelQuery::initfield:
        return QLatin1String("initfield");
        break;
    case ModelQuery::lclist:
        return QLatin1String("lclist");
        break;
    case ModelQuery::lcprops:
        return QLatin1String("lcprops");
        break;
    case ModelQuery::merge:
        return QLatin1String("merge");
        break;
    case ModelQuery::modify:
        return QLatin1String("modify");
        break;
    case ModelQuery::move:
        return QLatin1String("move");
        break;
    case ModelQuery::plan:
        return QLatin1String("plan");
        break;
    case ModelQuery::plantitle:
        return QLatin1String("plantitle");
        break;
    case ModelQuery::remove:
        return QLatin1String("remove");
        break;
    case ModelQuery::save:
        return QLatin1String("save");
        break;
    case ModelQuery::sclist:
        return QLatin1String("sclist");
        break;
    case ModelQuery::stlist:
        return QLatin1String("stlist");
        break;
    case ModelQuery::swlist:
        return QLatin1String("swlist");
        break;
    case ModelQuery::themes:
        return QLatin1String("themes");
        break;
    case ModelQuery::invalid:
        return QLatin1String("invalid");
        break;
    }

    return QLatin1String("");
}

static ModelQuery::Command fromString(const QString &value)
{
    if (value == QLatin1String("add")) {
        return ModelQuery::add;
    }
    if (value == QLatin1String("addmodule")) {
        return ModelQuery::addmodule;
    }
    if (value == QLatin1String("bklist")) {
        return ModelQuery::bklist;
    }
    if (value == QLatin1String("fstat")) {
        return ModelQuery::fstat;
    }
    if (value == QLatin1String("initfield")) {
        return ModelQuery::initfield;
    }
    if (value == QLatin1String("lclist")) {
        return ModelQuery::lclist;
    }
    if (value == QLatin1String("lcprops")) {
        return ModelQuery::lcprops;
    }
    if (value == QLatin1String("merge")) {
        return ModelQuery::merge;
    }
    if (value == QLatin1String("modify")) {
        return ModelQuery::modify;
    }
    if (value == QLatin1String("move")) {
        return ModelQuery::move;
    }
    if (value == QLatin1String("plan")) {
        return ModelQuery::plan;
    }
    if (value == QLatin1String("plantitle")) {
        return ModelQuery::plantitle;
    }
    if (value == QLatin1String("remove")) {
        return ModelQuery::remove;
    }
    if (value == QLatin1String("save")) {
        return ModelQuery::save;
    }
    if (value == QLatin1String("sclist")) {
        return ModelQuery::sclist;
    }
    if (value == QLatin1String("stlist")) {
        return ModelQuery::stlist;
    }
    if (value == QLatin1String("swlist")) {
        return ModelQuery::swlist;
    }
    if (value == QLatin1String("themes")) {
        return ModelQuery::themes;
    }
    return ModelQuery::invalid;
}

ModelQuery::ModelQuery()
    : mCommand(), mLocomotive(), mCommandValid(0), mLocomotiveValid(0)
{
}

ModelQuery::ModelQuery(Command pmCommand, const Locomotive &pmLocomotive)
    : mCommand(pmCommand), mLocomotive(pmLocomotive), mCommandValid(1), mLocomotiveValid(1)
{
}

ModelQuery::ModelQuery(const QDomNode &source)
    : mCommand(), mLocomotive()
{
    if (source.nodeName() == QString::fromLatin1("model"))
    {
        {
            const QString &attributeName(QString::fromLatin1("cmd"));
            if (source.attributes().contains(attributeName))
            {
                mCommand = fromString(source.attributes().namedItem(attributeName).nodeValue());
                mCommandValid = 1;
            }
        }
        {
            const QDomNodeList &allChilds(source.childNodes());
            const int childCount = allChilds.count();
            for (int i = 0; i < childCount; ++i) {
                if (allChilds.at(i).nodeName() == "lc") {
                    mLocomotive = Locomotive(allChilds.at(i));
                    break;
                }
            }
        }
    }
}

void ModelQuery::toXML(QDomDocument &document, QDomNode &rootNode) const
{
    QDomElement modelXmlQuery(document.createElement(QString::fromLatin1("model")));
    if (mCommandValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("cmd"), toString(command()));
    }
    mLocomotive.toXML(document, modelXmlQuery);
    rootNode.appendChild(modelXmlQuery);
}

void ModelQuery::fuse(const ModelQuery &other)
{
    if (other.mCommandValid) {
        setCommand(other.command());
    }
    if (other.mLocomotiveValid) {
        setLocomotive(other.locomotive());
    }
}

}

