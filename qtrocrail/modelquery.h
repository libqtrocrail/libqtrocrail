/**
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#if !defined MODELQUERY_H
#define MODELQUERY_H

#include "qtrocrail_export.h"

#include <locomotive.h>

#include <QtGlobal>
#include <QString>
#include <QMetaType>

class QDomDocument;
class QDomNode;

namespace QtRocrail
{

/**
 * Class that is used to represent a query or command about RocRail models.
 * 
 * It takes a Command parameter indicating which query or command is to be sent with this model block.
 */
class QTROCRAIL_EXPORT ModelQuery
{
public:
    enum Command
    {
        add, addmodule, bklist, fstat, initfield, lclist, lcprops, merge, modify, move, plan, plantitle, remove, save, sclist, stlist, swlist, themes, invalid
    };

    explicit ModelQuery(Command, const Locomotive&);

    explicit ModelQuery(const QDomNode&);

    ModelQuery();

    void toXML(QDomDocument&, QDomNode&) const;

    void fuse(const ModelQuery&);

    Command command() const
    {
        return mCommand;
    }

    void setCommand(Command pmCommand)
    {
        mCommand = pmCommand;
        mCommandValid = 1;
    }

    bool locomotiveValid() const
    {
        return mLocomotiveValid;
    }

    const Locomotive& locomotive() const
    {
        return mLocomotive;
    }

    void setLocomotive(const Locomotive &pmLocomotive)
    {
        mLocomotive = pmLocomotive;
    }

private:

    /**
     * The actual command set for this ModelQuery object.
     */
    Command mCommand;

    Locomotive mLocomotive;

    /**
     * Is the field mCommand is valid
     */
    int mCommandValid:1;

    /**
     * Is the field mLocomotive is valid
     */
    int mLocomotiveValid:1;

};

}

Q_DECLARE_METATYPE(QtRocrail::ModelQuery)

#endif

