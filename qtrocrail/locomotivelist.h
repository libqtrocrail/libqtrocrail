/**
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#if !defined LOCOMOTIVELIST_H
#define LOCOMOTIVELIST_H

#include "qtrocrail_export.h"

#include <locomotive.h>

#include <QList>
#include <QtGlobal>
#include <QString>
#include <QMetaType>

class QDomDocument;
class QDomNode;

namespace QtRocrail
{


class QTROCRAIL_EXPORT LocomotiveList
{
public:
    explicit LocomotiveList(const QList<Locomotive>&);

    explicit LocomotiveList(const QDomNode&);

    LocomotiveList();

    void toXML(QDomDocument&, QDomNode&) const;

    void fuse(const LocomotiveList&);

    const QList<Locomotive>& locomotivesList() const
    {
        return mLocomotivesList;
    }

    void setLocomotiveList(const QList<Locomotive> &pmLocomotivesList)
    {
        mLocomotivesList = pmLocomotivesList;
    }

private:

    QList<Locomotive> mLocomotivesList;

};

}

Q_DECLARE_METATYPE(QtRocrail::LocomotiveList)

#endif

