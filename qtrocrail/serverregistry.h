/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#if !defined SERVERREGISTRY_H
#define SERVERREGISTRY_H


#include <QObject>
#include <QSharedPointer>
#include <QString>
#include <QHash>

#include "serverstate.h"

#include "qtrocrail_export.h"

namespace QtRocrail
{

class QTROCRAIL_EXPORT ServerRegistry
{
public:

    explicit ServerRegistry();

    QSharedPointer<QtRocrail::ServerState> getComponent(const QString&, const QString&);

private:

    QHash<QString, QWeakPointer<QtRocrail::ServerState> > mExistingServers;

};

}

#endif // SERVERREGISTRY_H
