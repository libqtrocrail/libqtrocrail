/*
 *   Copyright 2010 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef NETWORKPROTOCOL_H
#define NETWORKPROTOCOL_H

#include <QObject>
#include <QDomDocument>
#include <QByteArray>
#include <QRegExp>
#include <QList>
#include <QIODevice>

#include <QHostAddress>

class QAbstractSocket;

#include "qtrocrail_export.h"

namespace QtRocrail
{

class RemoteQuery;

/**
 * Class that is used to manipulate the data exchanges with a rocrail server. It provides nice facilities to allow sending and getting back the answer.
 */
class QTROCRAIL_EXPORT NetworkTransport : public QObject
{
    Q_OBJECT

public:

    Q_PROPERTY(bool connected
               READ isConnected
               NOTIFY connectStateChanged
               )

    enum MessageType {
        lclist, lc, clocktick, model,
        invalid
    };

    explicit NetworkTransport(QAbstractSocket*, QObject* = 0);

    ~NetworkTransport();

    void connectToHost(const QString&, quint16, QIODevice::OpenMode = QIODevice::ReadWrite);

    void connectToHost(const QHostAddress&, quint16, QIODevice::OpenMode = QIODevice::ReadWrite);

    bool containsQuerys() const;

    QtRocrail::RemoteQuery readLastQuery() const;

    void popLastQuery();

    bool isConnected() const;

public slots:

    void sendQuery(const QtRocrail::RemoteQuery&) const;

signals:

    void receivedQuery();

    void connected();

    void disconnected();

    void connectStateChanged();

    void connectionError();

protected:

    QString buildHeader(unsigned int length) const;

private slots:

    void serverSocketStateChanged(QAbstractSocket::SocketState);

    void serverSocketStateInError(QAbstractSocket::SocketError);

    void serverSocketConnected();

    void serverSocketDisconnected();

    void dataReady();

private:

    QAbstractSocket *mDevice;

    bool mIsConnected;

    QRegExp mHeaderRegExp;

    QString mBuffer;

    int mCurrentMessageSize;

    QList<QtRocrail::RemoteQuery> receivedQuerys;
};

}

#endif // NETWORKPROTOCOL_H
