/*
 *   Copyright 2010 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#if !defined SERVERSTATE_H
#define SERVERSTATE_H

#include "locomotivelist.h"
#include "locomotive.h"
#include "modelquery.h"
#include "clocktick.h"

#include <QObject>
#include <QList>

class QAbstractSocket;
class QStateMachine;

#include "qtrocrail_export.h"

namespace QtRocrail
{

class NetworkTransport;
class RemoteQuery;

class QTROCRAIL_EXPORT ServerState : public QObject
{
    Q_OBJECT
public:

    Q_PROPERTY(bool connected
               READ isConnected
               NOTIFY connectStateChanged
               )

    explicit ServerState(const QString&, QObject *parent = 0);

    ~ServerState();

    bool isConnected() const;

signals:

    void connected();

    void disconnected();

    void connectStateChanged();

    void newLocomotiveMessage(const QtRocrail::Locomotive&);

    void newLocomotiveListMessage(const QtRocrail::LocomotiveList&);

    void newClockTickMessage(const QtRocrail::ClockTick&);

    void newModelQueryMessage(const QtRocrail::ModelQuery&);

public slots:

    void sendServerQuery(const QtRocrail::RemoteQuery&);

protected slots:

    void requestReceived();

    void doServerConnection();

    void reconnect();

private:

    void createConnectionToServer();

    void createStateMachine();

    QtRocrail::NetworkTransport *transport;

    QStateMachine *automaton;

    QString mServerAdress;

    qlonglong mCurrentTime;

};

}

#endif // SERVERSTATE_H
