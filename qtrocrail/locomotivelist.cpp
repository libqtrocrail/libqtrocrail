/**
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "locomotivelist.h"

#include <QDomDocument>
#include <QDomNode>
#include <QDebug>

namespace QtRocrail
{

LocomotiveList::LocomotiveList()
    : mLocomotivesList()
{
}

LocomotiveList::LocomotiveList(const QList<Locomotive> &pmLocomotivesList)
    : mLocomotivesList(pmLocomotivesList)
{
}

LocomotiveList::LocomotiveList(const QDomNode &source)
    : mLocomotivesList()
{
    if (source.nodeName() == QString::fromLatin1("lclist"))
    {
        {
            const QDomNodeList &allChilds(source.childNodes());
            const int childCount = allChilds.count();
            for (int i = 0; i < childCount; ++i) {
                mLocomotivesList.push_back(Locomotive(allChilds.at(i)));
            }
        }
    }
}

void LocomotiveList::toXML(QDomDocument &document, QDomNode &rootNode) const
{
    QDomElement modelXmlQuery(document.createElement(QString::fromLatin1("lclist")));
    for (QList<Locomotive>::const_iterator itElt = mLocomotivesList.begin(); itElt != mLocomotivesList.end(); ++itElt) {
        itElt->toXML(document, modelXmlQuery);
    }
    rootNode.appendChild(modelXmlQuery);
}

void LocomotiveList::fuse(const LocomotiveList &other)
{
}

}

