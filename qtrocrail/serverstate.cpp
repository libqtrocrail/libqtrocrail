/*
 *   Copyright 2010 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "serverstate.h"
#include "networkprotocol.h"
#include "remotequery.h"

#include <QTimer>
#include <QTcpSocket>
#include <QStateMachine>

namespace QtRocrail
{

ServerState::ServerState(const QString &serverAdress, QObject *parent) :
    QObject(parent), transport(0), automaton(0), mServerAdress(serverAdress)
{
    createConnectionToServer();
    createStateMachine();
}

ServerState::~ServerState()
{
    delete automaton;
    delete transport;
}

void ServerState::createStateMachine()
{
    automaton = new QStateMachine(this);
    QState *notConnectedState = new QState(automaton);
    connect(notConnectedState, SIGNAL(entered()), this, SLOT(reconnect()));
    QState *connectedState = new QState(automaton);
    connect(connectedState, SIGNAL(entered()), this, SIGNAL(connected()));
    connect(connectedState, SIGNAL(entered()), this, SIGNAL(connectStateChanged()));
    QState *connectingState = new QState(automaton);
    connect(connectingState, SIGNAL(entered()), this, SLOT(doServerConnection()));
    notConnectedState->addTransition(notConnectedState, SIGNAL(entered()), connectingState);
    connectingState->addTransition(transport, SIGNAL(connected()), connectedState);
    connectingState->addTransition(transport, SIGNAL(connectionError()), notConnectedState);
    connectedState->addTransition(transport, SIGNAL(disconnected()), notConnectedState);
    automaton->setInitialState(notConnectedState);
    automaton->start();
}

void ServerState::createConnectionToServer()
{
    transport = new NetworkTransport(new QTcpSocket(this), this);
    connect(transport, SIGNAL(receivedQuery()), this, SLOT(requestReceived()));
}

void ServerState::doServerConnection()
{
    qDebug() << "ServerState::doServerConnection()" << mServerAdress;
    transport->connectToHost(mServerAdress, 62842, QIODevice::ReadWrite);
}

void ServerState::reconnect()
{
    qDebug() << "ServerState::reconnect()";
    emit disconnected();
    emit connectStateChanged();
}

bool ServerState::isConnected() const
{
    return transport->isConnected();
}

void ServerState::requestReceived()
{
    qDebug() << "ServerState::requestReceived()";
    while(transport->containsQuerys()) {
        const RemoteQuery &currentReply = transport->readLastQuery();
        qDebug() << "last received query:" << currentReply.toString();
        switch (currentReply.getHeader()) {
        case NetworkTransport::lclist: {
            emit newLocomotiveListMessage(LocomotiveList(currentReply.xml().firstChild()));
            break;
        }
        case NetworkTransport::lc: {
            emit newLocomotiveMessage(Locomotive(currentReply.xml().firstChild()));
            break;
        }
        case NetworkTransport::clocktick: {
            emit newClockTickMessage(ClockTick(currentReply.xml().firstChild()));
            break;
        }
        case NetworkTransport::model: {
            emit newModelQueryMessage(ModelQuery(currentReply.xml().firstChild()));
            break;
        }
        }
        transport->popLastQuery();
    }
}

void ServerState::sendServerQuery(const RemoteQuery &query)
{
    transport->sendQuery(query);
}

}
