/*
 *   Copyright 2010 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef REMOTEQUERY_H
#define REMOTEQUERY_H

#include "qtrocrail_export.h"

#include "networkprotocol.h"

#include <QDomDocument>
#include <QString>
#include <QStringRef>

namespace QtRocrail
{

class QTROCRAIL_EXPORT RemoteQuery
{
public:
    explicit RemoteQuery(const QDomDocument&);

    explicit RemoteQuery(const QString&);

    explicit RemoteQuery(const QStringRef&);

    QString toString(int indentation = 1) const;

    const QDomDocument& xml() const;

    QtRocrail::NetworkTransport::MessageType getHeader() const;

    static QtRocrail::RemoteQuery fromString(const QString&);

    static QtRocrail::RemoteQuery fromStringRef(const QStringRef&);

    template <typename T>
    static QtRocrail::RemoteQuery fromQuery(const T&);

private:

    QDomDocument query;
};

template <typename T>
QtRocrail::RemoteQuery RemoteQuery::fromQuery(const T &query)
{
    QDomDocument theXml;
    query.toXML(theXml, theXml);
    RemoteQuery res(theXml);
    return res;
}

}

#endif // REMOTEQUERY_H
