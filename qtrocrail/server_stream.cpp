/*
 *   Copyright 2010 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "server_stream.h"

#include <QXmlStreamWriter>
#include <QLatin1String>
#include <QIODevice>
#include <QDebug>

namespace QtRocrail
{

QLatin1String toString(ModelQuery::Command cmd)
{
    switch (cmd)
    {
    case ModelQuery::add:
        return QLatin1String("add");
    case ModelQuery::addmodule:
        return QLatin1String("addmodule");
    case ModelQuery::bklist:
        return QLatin1String("bklist");
    case ModelQuery::fstat:
        return QLatin1String("fstat");
    case ModelQuery::initfield:
        return QLatin1String("initfield");
    case ModelQuery::lclist:
        return QLatin1String("lclist");
    case ModelQuery::lcprops:
        return QLatin1String("lcprops");
    case ModelQuery::merge:
        return QLatin1String("merge");
    case ModelQuery::modify:
        return QLatin1String("modify");
    case ModelQuery::move:
        return QLatin1String("move");
    case ModelQuery::plan:
        return QLatin1String("plan");
    case ModelQuery::plantitle:
        return QLatin1String("plantitle");
    case ModelQuery::remove:
        return QLatin1String("remove");
    case ModelQuery::save:
        return QLatin1String("save");
    case ModelQuery::sclist:
        return QLatin1String("sclist");
    case ModelQuery::stlist:
        return QLatin1String("stlist");
    case ModelQuery::swlist:
        return QLatin1String("swlist");
    case ModelQuery::themes:
        return QLatin1String("themes");
    }

    return QLatin1String("");
}

ServerStream::ServerStream(QIODevice *device) : mDevice(device)
{
}

ServerStream& ServerStream::operator <<(const ModelQuery &query)
{
    QString payload;
    QXmlStreamWriter thePayloadWriter(&payload);

    thePayloadWriter.writeStartElement(QString::fromLatin1("model"));
    thePayloadWriter.writeAttribute(QString::fromLatin1("cmd"), toString(query.command()));
    thePayloadWriter.writeEndElement();

    sendMessage(payload);

    return *this;
}

ServerStream& ServerStream::operator>>(QString &readData)
{
    mDevice->waitForReadyRead(5000);
    readData = QString(mDevice->readAll());
    return *this;
}

void ServerStream::sendMessage(const QString &payload)
{
    QString msg(buildHeader(payload.length()) + payload);
    mDevice->write(msg.toUtf8());
}

QString ServerStream::buildHeader(unsigned int length) const
{
    QString header;
    QXmlStreamWriter theWriter(&header);

    theWriter.writeStartDocument();
    theWriter.writeStartElement(QString::fromLatin1("xmlh"));
    theWriter.writeStartElement(QString::fromLatin1("xml"));
    theWriter.writeAttribute(QString::fromLatin1("size"), QString::number(length));
    theWriter.writeEndElement();
    theWriter.writeEndElement();

    return header;
}

}
